class BreedsController < ApplicationController

  def index
    @breeds = Breed.order('name ASC')
  end

  def new
    @breed = Breed.new
  end

  def create
    @breed = Breed.new breed_params
    if @breed.valid?
      @breed.save
      flash[:success] = 'Breed has been successfully found and saved!'
      redirect_to breeds_path
    else
      flash[:errors] = @breed.errors.full_messages
      render :new
    end
  end

  def show
    @breed = Breed.find params[:id]
  end


  private
  def breed_params
    params.require(:breed).permit(:name)
  end
end
