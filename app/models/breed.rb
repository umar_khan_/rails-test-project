class Breed < ActiveRecord::Base
  validates_presence_of :name, :pic_url
  validates_uniqueness_of :name, :pic_url

  before_validation :verify_breed

  private

  def verify_breed
    begin
      response = JSON.parse(RestClient.get("https://dog.ceo/api/breed/#{name}/images").body)
      self.pic_url = response["message"].first
    rescue Object => e
      errors.add(:name, 'No breed found with this name')
    end
  end

end
